package com.company;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class Generator {

    public static Shop generate() {

        List<Product>products = new ArrayList<>();
        products.add(new Product("strawberry", 100));
        products.add(new Product("onion", 100));
        products.add(new Product("milk", 100));
        products.add(new Product("potato", 100));

        Shop shop = new Shop("atb", products);

        return shop;

    }
}
