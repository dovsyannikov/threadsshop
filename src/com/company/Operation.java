package com.company;

import javax.swing.*;
import java.util.*;

public class Operation {

    Shop shop = Generator.generate();
    public List<Product> products = shop.getProducts();
    public List<Integer> stocks = Operation.getStockValues(products);

    public void minusOperation(){
        Operation.minusOneRandomProduct(products,stocks);
    }
    public void plusOperation(){
        Operation.plusOneRandomProduct(products,stocks);
    }

    public static void minusOneRandomProduct(List<Product> products, List<Integer> stockValues) {
        Random randomNum = new Random();

        for (int j = 0; j < 100; j++) {
            int min = 0;
            int max = products.size();
            int value = min + randomNum.nextInt(max);
            String key = products.get(value).getProductName();
            stockValues.set(value, stockValues.get(value) - 1);

            System.out.println(String.format("Buyer just bought 1 %s and only %d%n left in stock", key, stockValues.get(value)));
        }
    }
    public static void plusOneRandomProduct(List<Product> products, List<Integer> stockValues) {
        Random randomNum = new Random();

        for (int j = 0; j < 100; j++) {
            int min = 0;
            int max = products.size();
            int value = min + randomNum.nextInt(max);
            String key = products.get(value).getProductName();
            stockValues.set(value, stockValues.get(value) + 1);

            System.out.println(String.format("Warehouse just bring to us 1 %s and now the stock of that product is %d%n", key, stockValues.get(value)));
        }
    }
    public static List <Integer> getStockValues(List<Product> products){
        List<Integer>stockValues = new ArrayList<>();
        for (int i = 0; i < products.size(); i++) {
            stockValues.add(products.get(i).getProductQuantity());
        }
        return stockValues;
    }
}
