package com.company;

import java.util.*;

public class Main {

    static Object monitor = new Object();

    public static void update(){
        synchronized (Main.class){

        }
    }


    public static void main(String[] args) {
        new MyThread().start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                Operation operation = new Operation();
                operation.plusOperation();
            }
        }).start();
        System.out.println("Main is finished");
    }
    public static class MyThread extends Thread{
        @Override
        public void run() {
            Operation operation = new Operation();
            operation.minusOperation();
        }
    }
}
