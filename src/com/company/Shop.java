package com.company;

import java.util.List;

public class Shop {

    private String shopName;
    private List<Product> products;

    public Shop(String shopName, List<Product> products) {
        this.shopName = shopName;
        this.products = products;
    }

    public List<Product> getProducts() {
        return products;
    }
}
